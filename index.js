class Tab {
  tabItemsArray = []
  static tabContentArray = []
  constructor(tabArray) {
    this.tabItemsArray = tabArray;
  }

  render() {




    let tab = document.getElementById('tab')
    let tabItems = document.createElement('div');

    tabItems.classList.add(`tab__items`)

    this.tabItemsArray.forEach((tabItem, index) => {
      let tabItemDiv = document.createElement(`div`);
      tabItemDiv.classList.add('tab__item')
      tabItemDiv.dataset.ind = index;

      let tabTitle = tabItem.title
      Tab.tabContentArray.push(tabItem.data)

      tabItemDiv.innerText = tabTitle
      tabItems.appendChild(tabItemDiv)
    })

    return tabItems
  }
}

document.addEventListener(`click`, (event) => {
  if (event.target.classList.contains(`tab__item`)) {
    // понять какой это элемент через дата атрибут и отрендерить его контент взяв контент по индексу из массива

    // провеить
    let withCur = document.querySelectorAll(`._current`)
    withCur.forEach(item => {
      item.classList.remove('_current');
    })

    event.target.classList.add('_current')



    let index = event.target.dataset.ind;

    let tabContent = document.querySelector(`#tabContent`)
    if (tabContent) {
      tabContent.innerText = Tab.tabContentArray[index]
    }
    else {
      let tabContent = document.createElement(`div`)
      tabContent.classList.add(`tab__content`)
      tabContent.id = `tabContent`;
      tabContent.innerText = Tab.tabContentArray[index];
      document.getElementById(`tab`).appendChild(tabContent)
    }
  }
})

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])
console.log(tab)


// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render())

// tab.render()